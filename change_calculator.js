/*
Buatlah function yang dapat digunakan oleh kasir untuk menghitung nilai uang kembalian beserta dengan pecahan uang yang bisa diberikan. Input berupa total belanja dan jumlah uang yang dibayarkan oleh pembeli. Output berupa kembalian (dibulatkan ke bawah Rp.100) yang harus diberikan kasir dengan detail pecahan uang yang harus diberikan. Pecahan yang tersedia adalah: 100.000, 50.000, 20.000, 10.000, 5.000, 2.000, 1.000, 500, 200, 100.

Kembalikan nilai False apabila jumlah uang yang dibayarkan kurang dari total belanjanya.
*/

function mathHelper(divident, divisor) {
  return [Math.floor(divident / divisor), divident % divisor];
}

function calculateChange(total, cash) {
  if (cash < total) {
    return false;
  } else {
    const hundredthous = mathHelper(cash - total, 100000);
    const fiftythous = mathHelper(hundredthous[1], 50000);
    const twentythous = mathHelper(fiftythous[1], 20000);
    const tenthous = mathHelper(twentythous[1], 10000);
    const fivethous = mathHelper(tenthous[1], 5000);
    const twothous = mathHelper(fivethous[1], 2000);
    const onethou = mathHelper(twothous[1], 1000);
    const fivehun = mathHelper(onethou[1], 500);
    const twohun = mathHelper(fivehun[1], 200);
    const onehun = mathHelper(twohun[1], 100);

    const summary = `
    Total: Rp. ${total}
    Cash: Rp. ${cash}
    
    Change:
       Rp. 100,000 : ${hundredthous[0]} pcs,
       Rp. 50,000 : ${fiftythous[0]} pcs,
       Rp. 20,000 : ${twentythous[0]} pcs,
       Rp. 10,000 : ${tenthous[0]} pcs,
       Rp. 5,000 : ${fivethous[0]} pcs,
       Rp. 2,000 : ${twothous[0]} pcs,
       Rp. 1,000 : ${onethou[0]} pcs,
       Rp. 500 : ${fivehun[0]} pcs,
       Rp. 200 : ${twohun[0]} pcs,
       Rp. 100 : ${onehun[0]} pcs
    `;

    return summary;
  }
}

console.log(calculateChange(455500, 700000));
console.log(calculateChange(455500, 10000));
